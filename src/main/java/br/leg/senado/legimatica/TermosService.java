package br.leg.senado.legimatica;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.leg.senado.legimatica.sigen.model.Termo;
import br.leg.senado.legimatica.sigen.parsers.ListaTermosXmlParser;
import lombok.extern.slf4j.Slf4j;

@Component
@RestController
@RequestMapping("/api/termos")
@Slf4j
public class TermosService {
	
	@Autowired
	private ListaTermosXmlParser termosParser;
	
	private List<Termo> listaTermos;
	private Map<Long, Termo> termosPorId;

	@GetMapping(path = "/termos", produces = "application/json")
	public List<Termo> getListaTermos(
			@RequestParam(value="startsWith", required=false) String startsWith, 
			@RequestParam(value="limit", required=false) Long limit) {
		if (startsWith == null && limit == null) {
			return this.getListaTermos();
		}
		Stream<Termo> stream = this.getListaTermos().stream();
		if (startsWith != null) {
			stream = stream.filter(t -> t.getTexto().startsWith(startsWith));
		}
		if (limit != null) {
			stream.limit(limit);
		}
		return stream.collect(Collectors.toList());
	}
	
	public List<Termo> getListaTermos() {
		if (this.listaTermos == null) {
			log.debug("Lendo listagem de termos...");
			try (InputStream stream = getListaTermosXmlStream()) {
				this.listaTermos = termosParser.parse(stream);
				log.debug(this.listaTermos.size() + " termos carregados.");
			} catch (IOException e) {
				throw new TermosServiceException(e);
			}
		}
		return this.listaTermos;
	}
	
	public Termo localizaTermoPorId(Long id) {
		return this.getTermosPorId().get(id);
	}
	
	protected Map<Long, Termo> getTermosPorId() {
		if (this.termosPorId == null) {
			List<Termo> termos = getListaTermos();
			this.termosPorId = new HashMap<>(termos.size());
			for (Termo termo : termos) {
				this.termosPorId.put(termo.getId(), termo);
			}
		}
		return termosPorId;
	}

	protected InputStream getListaTermosXmlStream() {
		return Thread.currentThread().getContextClassLoader().getResourceAsStream("sigen.xml");
	}

	@SuppressWarnings("serial")
	static class TermosServiceException extends RuntimeException {
		TermosServiceException(Throwable t) {
			super(t.getMessage(), t);
		}
	}

}
