package br.leg.senado.legimatica.sigen.parsers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import br.leg.senado.legimatica.sigen.model.Termo;

@Component
public class ListaTermosXmlParser {
	public List<Termo> parse(InputStream xmlStream) {
		try {
			ListaTermosXmlHandler handler = new ListaTermosXmlHandler();
			XMLReader parser = XMLReaderFactory.createXMLReader();
			parser.setContentHandler(handler);
			InputSource source = new InputSource(xmlStream);
			parser.parse(source);
			return handler.getTermos();
		} catch (IOException | SAXException e) {
			throw new ListaTermosParsingException(e);
		} finally {
		}
	}

	@SuppressWarnings("serial")
	static class ListaTermosParsingException extends RuntimeException {
		ListaTermosParsingException(Throwable cause) {
			super(cause.getMessage(), cause);
		}
	}
}

class ListaTermosXmlHandler extends DefaultHandler {
	private static final String ELEMENTO_TERMO = "m";
	private static final String ELEMENTO_ID = "I";
	private static final String ELEMENTO_N = "N";
	private static final String ELEMENTO_TEXTO = "T";
	private static final String ELEMENTO_P = "P";
	private static final String ELEMENTO_S = "S";

	private ArrayList<Termo> listaTermos = new ArrayList<>();
	private Stack<String> elementStack = new Stack<>();
	private Stack<Termo> objectStack = new Stack<>();

	public void startDocument() throws SAXException {
	}

	public void endDocument() throws SAXException {
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		this.elementStack.push(qName);
		if (ELEMENTO_TERMO.equals(qName)) {
			Termo termo = new Termo();
			this.objectStack.push(termo);
		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		this.elementStack.pop();
		if (ELEMENTO_TERMO.equals(qName)) {
			Termo object = this.objectStack.pop();
			this.listaTermos.add(object);
		}
	}

	/**
	 * This will be called everytime parser encounter a value node
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = new String(ch, start, length).trim();
		if (this.objectStack.isEmpty() || value.length() == 0) {
			return; // ignore white space
		}

		// handle the value based on to which element it belongs
		Termo termo = (Termo) this.objectStack.peek();
		switch (currentElement()) {
		case ELEMENTO_ID:
			termo.setId(Long.valueOf(value));
			break;
		case ELEMENTO_TEXTO:
			termo.setTexto(value);
			break;
		case ELEMENTO_N:
			termo.setN(value);
			break;
		case ELEMENTO_P:
			termo.setP(value);
			break;
		case ELEMENTO_S:
			termo.setS(value);
			break;
		default:
			/* ignora */;
		}
	}

	/**
	 * Utility method for getting the current element in processing
	 */
	private String currentElement() {
		return this.elementStack.peek();
	}

	public List<Termo> getTermos() {
		return listaTermos;
	}
}
