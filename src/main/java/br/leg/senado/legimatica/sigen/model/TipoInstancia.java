package br.leg.senado.legimatica.sigen.model;

import lombok.Data;

@Data
public class TipoInstancia {
	Long id;
	String descricao;
}
