package br.leg.senado.legimatica.sigen.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Norma {
	private String nomePreferido;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dataAssinatura;
	private String nomeArquivo;
	private Long codDocumento;
	private Long codPublicacao;
	private Long codPropArqBinario;
	
	public String getTipo() {
		return splitArquivoNorma()[0];
	}

	public Integer getAno() {
		return Integer.valueOf(splitArquivoNorma()[2]);
	}

	public String getNumero() {
		return splitArquivoNorma()[1];
	}
	
	// Retorna um array contendo, na ordem: tipo, numero e ano da norma.
	private String[] splitArquivoNorma() {
		String[] partesNomeArquivo = this.nomeArquivo.split("-");		
		if (partesNomeArquivo.length == 6) {
			// Nome do arquivo tem o formato TIPO-NUMERO-ANO-MES-DIA-PUB
			return new String[] {
					partesNomeArquivo[0],
					partesNomeArquivo[1],
					partesNomeArquivo[2]
			};
		} 
		if (partesNomeArquivo.length == 7) {
			// Nome do arquivo tem o formato TIPO-NUMERO-LETRA-ANO-MES-DIA, ex. LEI-12345-A-2016-10-22-PUB
			return new String[] { 
					partesNomeArquivo[0], 
					partesNomeArquivo[1] + '-' + partesNomeArquivo[2], 
					partesNomeArquivo[3]
			};
		}
		throw new IllegalStateException("Nome do arquivo não tem o formato esperado: " + this.nomeArquivo);		
	}
}
