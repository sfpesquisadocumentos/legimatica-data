package br.leg.senado.legimatica.sigen.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data @EqualsAndHashCode(of="id")
public class Termo {
	String s;
	String texto;
	String n;
	Long id;
	String p;
}
