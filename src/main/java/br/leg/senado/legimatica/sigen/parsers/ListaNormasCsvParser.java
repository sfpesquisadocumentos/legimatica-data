package br.leg.senado.legimatica.sigen.parsers;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import br.leg.senado.legimatica.sigen.model.Norma;

@Component
public class ListaNormasCsvParser {

	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	@SuppressWarnings("serial")
	static class ListaLeisParsingException extends RuntimeException {
		ListaLeisParsingException(Throwable cause) {
			super(cause.getMessage(), cause);
		}
	}

	public List<Norma> parse(InputStream csvStream) {
		List<Norma> result = new ArrayList<>();
		CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
		CSVReader reader = new CSVReaderBuilder(new InputStreamReader(csvStream))
				.withSkipLines(1).withCSVParser(parser).build();
		try {
            String[] line;
            while ((line = reader.readNext()) != null) {
            	Norma norma = new Norma();
            	norma.setNomePreferido(line[0]);
            	norma.setDataAssinatura(df.parse(line[1]));
            	norma.setNomeArquivo(line[2]);
            	norma.setCodDocumento(Long.valueOf(line[3]));
            	norma.setCodPublicacao(Long.valueOf(line[4]));
            	norma.setCodPropArqBinario(Long.valueOf(line[5]));
            	result.add(norma);
            }
		} catch (Exception e) {
			throw new ListaLeisParsingException(e);
		}
		return result;
	}
}
