package br.leg.senado.legimatica.sigen.model;

import lombok.Data;

@Data
public class VersaoInstancia {
	Long id;
	String descricao;
	String agente;
	/*Date*/ String timestamp; // TODO Arrumar. Problema de desserialização do Timestamp.
}
