package br.leg.senado.legimatica.sigen;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import br.leg.senado.legimatica.sigen.model.InstanciaCompacta;
import br.leg.senado.legimatica.sigen.model.InstanciaRef;
import br.leg.senado.legimatica.sigen.parsers.InstanciaCompactaJsonParser;
import br.leg.senado.legimatica.sigen.parsers.InstanciaRefJsonParser;
import br.leg.senado.legimatica.util.GenericJsonProcessor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SigenService {
		
	public InstanciaCompacta getDetalheInstancia(Long id) {
		String address = String.format("https://legis.senado.gov.br/sigen/api/instancia/%s/compacta", id);
		return getDetalheInstanciaFromUrl(address);
	}

	public <T> T getPropriedade(Long id, String nomePropriedade, Class<T> classePropriedade) {
		String address = String.format("https://legis.senado.gov.br/sigen/api/instancia/%s/propriedade/%s/compacta", id, nomePropriedade);

		log.debug("Lendo propriedade de instância, URL '{}'...", address);
		try (InputStream stream = new URL(address).openStream()) {
			return new GenericJsonProcessor().parseProperty(stream, nomePropriedade, classePropriedade);
		} catch (FileNotFoundException e) {
			return (T) null;
		} catch (IOException e) {
			throw new SigenServiceException(e);
		}
	}

	public List<InstanciaRef> getPropriedadeInstanciaRef(Long id, String nomePropriedade) {
		String address = String.format("https://legis.senado.gov.br/sigen/api/instancia/%s/propriedade/%s/compacta", id, nomePropriedade);
		return getPropriedadeInstanciaFromUrl(address);
	}

	@Cacheable("detalheInstanciaPorUrl")
	public InstanciaCompacta getDetalheInstanciaFromUrl(String address) {
		log.debug("Lendo detalhe de instância, URL '{}'...", address);
		try (InputStream stream = new URL(address).openStream()) {
			InstanciaCompacta instancia = new InstanciaCompactaJsonParser().parse(stream);
			log.debug("Lida instância '{}'", instancia.getDescricao());
			return instancia;
		} catch (IOException e) {
			throw new SigenServiceException(e);
		}
	}

	@Cacheable("propriedadeInstanciaPorUrl")
	public List<InstanciaRef> getPropriedadeInstanciaFromUrl(String address) {
		log.debug("Lendo propriedade de instância, URL '{}'...", address);
		try (InputStream stream = new URL(address).openStream()) {
			return new InstanciaRefJsonParser().parseList(stream);
		} catch (FileNotFoundException e) {
			return Collections.emptyList();
		} catch (IOException e) {
			throw new SigenServiceException(e);
		}
	}

	@SuppressWarnings("serial")
	static class SigenServiceException extends RuntimeException {
		SigenServiceException(Throwable t) {
			super(t.getMessage(), t);
		}
	}
}
