package br.leg.senado.legimatica.sigen.model;

import lombok.Data;

@Data
public class InstanciaRef {
	String name;
	String title;
	String href;
}
