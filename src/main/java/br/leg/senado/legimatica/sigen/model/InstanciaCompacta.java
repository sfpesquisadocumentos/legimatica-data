package br.leg.senado.legimatica.sigen.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InstanciaCompacta {
	Long id;
	TipoInstancia tipo;
	String descricao;
	String nomeDeTermo;
	String siglaDeTermo;
	@JsonProperty("_links")
	Map<String, List<InstanciaRef>> links;
}
