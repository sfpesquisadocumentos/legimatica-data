package br.leg.senado.legimatica.sigen.parsers;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.stereotype.Component;

import br.leg.senado.legimatica.sigen.model.InstanciaCompacta;
import br.leg.senado.legimatica.util.GenericJsonProcessor;

@Component
public class InstanciaCompactaJsonParser extends GenericJsonProcessor {
	
	public InstanciaCompacta parse(InputStream jsonStream) {
		InstanciaCompacta result = null;
		try {
			result = getObjectMapper().readValue(jsonStream, InstanciaCompacta.class);
		} catch (IOException e) {
			throw new JsonProcessingException(e);
		}
		return result;
	}	
}

