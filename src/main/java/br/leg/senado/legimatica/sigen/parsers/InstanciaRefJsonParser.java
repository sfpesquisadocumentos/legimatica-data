package br.leg.senado.legimatica.sigen.parsers;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.leg.senado.legimatica.sigen.model.InstanciaRef;
import br.leg.senado.legimatica.util.GenericJsonProcessor;

@Component
public class InstanciaRefJsonParser extends GenericJsonProcessor {

	public InstanciaRef parse(InputStream jsonStream) {
		InstanciaRef result = null;
		try {
			result = getObjectMapper().readValue(jsonStream, InstanciaRef.class);
		} catch (IOException e) {
			throw new JsonProcessingException(e);
		}
		return result;
	}

	public List<InstanciaRef> parseList(InputStream jsonStream) {
		try {
			ObjectMapper mapper = getObjectMapper();
			Propriedade parsed = mapper.readValue(jsonStream, Propriedade.class); 
			return parsed.values().iterator().next();
		} catch (IOException e) {
			throw new JsonProcessingException(e);
		}
	}

	static class Propriedade extends HashMap<String, List<InstanciaRef>> {
		private static final long serialVersionUID = 1L;
	}
}

