package br.leg.senado.legimatica.corpus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(value = "legimatica.corpus.location", havingValue = "local", matchIfMissing = true)
public class CorpusBrowserLocal extends CorpusBrowser {

	@Override
	public List<String> listaNormas(Integer anoNorma) {
		if (anoNorma == null) {
			return listaNormas();
		}		
		String path = String.format(PATH_TXT_NORMAS_AGRUPADAS_ANO, anoNorma);
		File pasta = new File(path);
		return Arrays.asList(pasta.listFiles()).stream()
			.filter(f -> f.isDirectory() && f.getName().contains("-")) // pasta da norma no formato TIPO-ANO-NUMERO
			.map(File::getName)
			.sorted()
			.collect(Collectors.toList());
	}

	protected List<String> listaNormas() {
		String pathNormas = PATH_TXT_NORMAS_AGRUPADAS;
		String[] anos = new File(pathNormas).list();
		List<String> lista = new ArrayList<>();
		for (String ano : anos) {
			if (ano.matches("\\d+")) {
				lista.addAll(listaNormas(Integer.valueOf(ano)));
			}
		}
		return lista;
	}

	@Override
	public List<String> listaDispositivos(Integer anoNorma, String tipoNorma, String numeroNorma) {
		String path = String.format(PATH_JSON_SENTENCAS, anoNorma, tipoNorma, anoNorma, formataNumeroNorma(numeroNorma));
		try {
			File pasta = new File(path);
			return Arrays.asList(pasta.listFiles()).stream()
				.filter(File::isFile)
				.map(File::getName)
				.sorted()
				.collect(Collectors.toList());
		} catch (Exception e) {
			throw new CorpusBrowserException(String.format("Erro ao listar '%s': %s", path, e.getMessage()), e);
		}
	}

	@Override
	public String getTxtEmenta(Integer anoNorma, String tipoNorma, String numeroNorma) {
		String path = String.format(PATH_TXT_NORMA, anoNorma, tipoNorma, anoNorma, formataNumeroNorma(numeroNorma));
		try {
			File pasta = new File(path);
			String[] lista = pasta.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.contains("ementa");
				}
			});
			
			try (InputStream sftpStream = new FileInputStream(path + "/" + lista[0])) {
				return IOUtils.toString(sftpStream, Charset.defaultCharset());
			}
		} catch (Exception e) {
			throw new CorpusBrowserException(String.format("Erro ao buscar ementa em '%s': %s", path, e.getMessage()), e);
		}
	}
	
	protected InputStream getContent(String path) {
		try {
			return new FileInputStream(path);
		} catch (Exception e) {
			throw new CorpusBrowserException("Erro ao executar getContent(" + path + "): " + e.getMessage(), e);
		}
	}


}
