package br.leg.senado.legimatica.corpus;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Component
@ConditionalOnProperty(value = "legimatica.corpus.location", havingValue = "remote")
public class CorpusBrowserRemote extends CorpusBrowser {

	private JSch jsch = new JSch();
	private Session session = null;
	
	@Value("${legimatica.server.username}")
	private String username;
	
	@Value("${legimatica.server.password}")
	private String password;
	
	@Value("${legimatica.server.host:pesquisa-textual.senado.gov.br}")
	private String host;

	@Value("${legimatica.server.port:22")
	private String port;
	
	@PostConstruct
	public void init() {
		if (session != null) {
			return;
		}
		try {
			session = jsch.getSession(username, host, 22);
		} catch (JSchException e) {
			throw new CorpusBrowserException(e);
		}
		session.setPassword(password);
		Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
	}
	
	@PreDestroy
	public void destroy() {
		if (session != null && session.isConnected()) {
			session.disconnect();
		}
	}

	@Override
	public List<String> listaNormas(Integer anoNorma) {
		if (anoNorma == null) {
			return listaNormas();
		}		
		SftpCommand<List<String>> cmdLista = sftp -> {
			String path = String.format(PATH_TXT_NORMAS_AGRUPADAS_ANO, anoNorma);
			try {
				@SuppressWarnings("unchecked")
				Vector<ChannelSftp.LsEntry> files = (Vector<ChannelSftp.LsEntry>) sftp.ls(path);
				List<String> arquivosNormas = files.stream()
					.filter(f -> f.getAttrs().isDir() && f.getFilename().contains("-") ) // pasta da norma no formato TIPO-ANO-NUMERO
					.map(ChannelSftp.LsEntry::getFilename)
					.sorted()
					.collect(Collectors.toList());
				return arquivosNormas;
			} catch (Exception e) {
				throw new CorpusBrowserException(String.format("Erro ao executar 'ls %s': %s", path, e.getMessage()), e);
			}
		};
		return runSftp(cmdLista);
	}

	protected List<String> listaNormas() {
		SftpCommand<List<String>> cmdLista = sftp -> {
			String path = PATH_TXT_NORMAS_AGRUPADAS;
			try {
				@SuppressWarnings("unchecked")
				Vector<ChannelSftp.LsEntry> files = (Vector<ChannelSftp.LsEntry>) sftp.ls(path);
				return files.stream()
					.filter(f -> f.getAttrs().isDir() && f.getFilename().matches("\\d+")) // pastas com numero do ano
					.map(ChannelSftp.LsEntry::getFilename)
					.sorted()
					.collect(Collectors.toList());
			} catch (Exception e) {
				throw new CorpusBrowserException(String.format("Erro ao executar 'ls %s': %s", path, e.getMessage()), e);
			}
		};
		List<String> anos = runSftp(cmdLista);
		
		List<String> lista = new ArrayList<>();
		for (String ano : anos) {
			lista.addAll(listaNormas(Integer.valueOf(ano)));
		}
		return lista;
	}
	
	@Override
	public List<String> listaDispositivos(Integer anoNorma, String tipoNorma, String numeroNorma) {
		SftpCommand<List<String>> cmdLista = (sftp) -> {
			String path = String.format(PATH_JSON_SENTENCAS, anoNorma, tipoNorma, anoNorma, formataNumeroNorma(numeroNorma));
			try {
				@SuppressWarnings("unchecked")
				Vector<ChannelSftp.LsEntry> files = (Vector<ChannelSftp.LsEntry>) sftp.ls(path);
				return files.stream()
					.filter(f -> !f.getAttrs().isDir() )
					.map(ChannelSftp.LsEntry::getFilename)
					.sorted()
					.collect(Collectors.toList());
			} catch (Exception e) {
				throw new CorpusBrowserException(String.format("Erro ao executar 'ls %s': %s", path, e.getMessage()), e);
			}
		};
		return runSftp(cmdLista);
	}
	
	@Override
	public String getTxtEmenta(Integer anoNorma, String tipoNorma, String numeroNorma) {
		SftpCommand<String> cmd = (sftp) -> {
			String path = String.format(PATH_TXT_NORMA, anoNorma, tipoNorma, anoNorma, formataNumeroNorma(numeroNorma));
			try {
				final List<String> filename = new ArrayList<>();
				LsEntrySelector selector = (entry) -> {
					if (entry.getFilename().contains("ementa")) {
						filename.add(entry.getFilename());
						return LsEntrySelector.BREAK;
					}
					return LsEntrySelector.CONTINUE;
				};
				sftp.ls(path, selector);
				
				try (InputStream sftpStream = sftp.get(path + "/" + filename.get(0))) {
					return IOUtils.toString(sftpStream, Charset.defaultCharset());
				}
			} catch (Exception e) {
				throw new CorpusBrowserException(String.format("Erro ao executar 'ls %s': %s", path, e.getMessage()), e);
			}
		};
		return runSftp(cmd);		
	}

	protected InputStream getContent(String path) {
		SftpCommand<InputStream> cmdGetContent = (sftp) -> {
			ByteArrayOutputStream inMemoryStream = null;
			try (InputStream sftpStream = sftp.get(path)) {
				inMemoryStream = new ByteArrayOutputStream();
				IOUtils.copyLarge(sftpStream, inMemoryStream);
				return new ByteArrayInputStream(inMemoryStream.toByteArray());
			} catch (Exception e) {
				throw new CorpusBrowserException("Erro ao executar getContent(" + path + "): " + e.getMessage(), e);
			} finally {
				IOUtils.closeQuietly(inMemoryStream); 
			}
		};
		return runSftp(cmdGetContent);
	}
	
	protected interface SftpCommand<T> {
		T run(ChannelSftp sftp);
	}
	
	protected <X> X runSftp(SftpCommand<X> cmd) {
		ChannelSftp sftp = null;
		try {
			sftp = (ChannelSftp) getSession().openChannel("sftp");
			sftp.connect();
			return cmd.run(sftp);
		} catch (JSchException e) {
			throw new CorpusBrowserException(e);
		} finally {
			if (sftp != null && sftp.isConnected()) {
				sftp.disconnect();
			}
		}
	}

	protected Session getSession() {
		if (this.session == null) {
			init();
		}
		if (!session.isConnected()) {
			try { 
				session.connect();
			} catch (JSchException e) {
				throw new CorpusBrowserException(e);
			}
		}
		return this.session;
	}
}
