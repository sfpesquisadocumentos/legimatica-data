package br.leg.senado.legimatica.corpus;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

public abstract class CorpusBrowser {

	protected static final String PATH_BASE = "/opt/normas";
	protected static final String PATH_TXT_NORMAS_AGRUPADAS = PATH_BASE + "/txt-normas";
	protected static final String PATH_TXT_NORMAS_AGRUPADAS_ANO = PATH_TXT_NORMAS_AGRUPADAS + "/%1$s";
	protected static final String PATH_TXT_EMENTA = PATH_TXT_NORMAS_AGRUPADAS_ANO + "/%2$s/%2$s-ementa.txt"; 
	protected static final String PATH_TXT_NORMA = PATH_BASE + "/txt/%s/%s-%s-%s";
	protected static final String PATH_JSON_SENTENCAS = PATH_BASE + "/txt-sentencas-json/%s/%s-%s-%s";
	protected static final String PATH_JSON_DISPOSITIVO = PATH_JSON_SENTENCAS + "/%s";
	protected static final String PATH_JSON_EMENTA = PATH_BASE + "/txt-normas-json/%1$s/%2$s-%1$s-%3$s/%2$s-%1$s-%3$s-ementa.json";
	protected static final String PATH_JSON_NORMA = PATH_BASE + "/txt-normas-json/%1$s/%2$s-%1$s-%3$s/%2$s-%1$s-%3$s-dispositivos.json";	
	
	public abstract List<String> listaNormas(Integer anoNorma);
	public abstract List<String> listaDispositivos(Integer anoNorma, String tipoNorma, String numeroNorma);
	public abstract String getTxtEmenta(Integer anoNorma, String tipoNorma, String numeroNorma);
	protected abstract InputStream getContent(String path);

	public Map<String, String> listaNormasComEmentas(Integer ano) {
		List<String> normas = listaNormas(ano);
		Map<String, String> ementas = new LinkedHashMap<>();
		for (String norma : normas) {
			String[] nomeArquivo = norma.split("-");		
			Integer anoNorma = Integer.valueOf(nomeArquivo[1]);

			InputStream is = getContent(String.format(PATH_TXT_EMENTA, anoNorma, norma));
			try {
				String ementa = IOUtils.toString(is, Charset.forName("UTF-8"));
				ementas.put(norma, ementa);
			} catch (IOException e) {
				throw new CorpusBrowserException("Erro ao buscar ementa da norma " + norma, e);
			} finally {
				IOUtils.closeQuietly(is);
			}
		}
		return ementas;
	}	
	
	public InputStream getJsonNorma(Integer anoNorma, String tipoNorma, String numeroNorma) {
		String path = String.format(PATH_JSON_NORMA, anoNorma, tipoNorma, formataNumeroNorma(numeroNorma));
		return getContent(path);
	}	
	
	public InputStream getJsonEmenta(Integer anoNorma, String tipoNorma, String numeroNorma) {
		String path = String.format(PATH_JSON_EMENTA, anoNorma, tipoNorma, formataNumeroNorma(numeroNorma));
		return getContent(path);
	}
	
	public InputStream getJsonDispositivo(Integer anoNorma, String tipoNorma, String numeroNorma, String idDispositivo) {
		if (!idDispositivo.endsWith(".json")) {
			idDispositivo = idDispositivo + ".json";
		}
		String path = String.format(PATH_JSON_DISPOSITIVO, anoNorma, tipoNorma, anoNorma, formataNumeroNorma(numeroNorma), idDispositivo);
		return getContent(path);
	}

	public List<String> getTextoDispositivos(Integer ano, String tipo, String numero) {
		List<String> listaDispositivos = listaDispositivos(ano, tipo, numero);
		List<String> textos = new ArrayList<>();
		for (String dispositivo : listaDispositivos) {
			String path = String.format("%s/txt-sentencas/%s/%s-%s-%s/%s", PATH_BASE, ano, 
					tipo, ano, formataNumeroNorma(numero), dispositivo.replaceAll(".json", ".txt"));
			try (InputStream stream = getContent(path)) {
				textos.add(IOUtils.toString(stream, Charset.defaultCharset()));
			} catch (IOException e) {
				throw new CorpusBrowserException(String.format("Erro ao buscar dispositivo em '%s': %s", path, e.getMessage()), e);
			}
		}
		return textos;
	}

	protected String formataNumeroNorma(String numeroNorma) {
		if (numeroNorma.contains("-")) {
			String[] partes = numeroNorma.split("-");
			return String.format("%05d-%s", Integer.parseInt(partes[0]), partes[1]);
		} else {
			return String.format("%05d", Integer.parseInt(numeroNorma));
		}
	}
	

}