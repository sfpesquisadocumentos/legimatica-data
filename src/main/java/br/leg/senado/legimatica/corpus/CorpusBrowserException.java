package br.leg.senado.legimatica.corpus;

public class CorpusBrowserException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public CorpusBrowserException(Throwable cause) {
		super(cause.getMessage(), cause);
	}

	public CorpusBrowserException(String message, Throwable cause) {
		super(message, cause);
	}
}