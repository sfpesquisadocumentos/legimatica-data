package br.leg.senado.legimatica.googlenl.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SyntaxTree {

	private List<SyntaxNode> roots = new ArrayList<>();
	
	public static SyntaxTree from(GoogleTextAnalysis textAnalysis) {
		SyntaxTree tree = new SyntaxTree();
		List<SyntaxNode> nodes = new ArrayList<>();
		for (Token token : textAnalysis.getTokens()) {
			nodes.add(new SyntaxNode(token));
		}
		for (SyntaxNode node : nodes) {
			DependencyEdge dep = node.getToken().getDependencyEdge();
			node.setHeadLabel(dep.getLabel());
			
			SyntaxNode headNode = nodes.get(dep.getHeadTokenIndex());
			if (node.equals(headNode)) /* ROOT */ {
				tree.getRoots().add(node);
			} else {
				node.setHead(headNode);
				headNode.getChildren().add(node);
			}
		}
		return tree;
	}
}
