package br.leg.senado.legimatica.googlenl.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data @RequiredArgsConstructor @EqualsAndHashCode(exclude="head") @ToString(exclude="head")
public class SyntaxNode {
	final Token token;
	SyntaxNode head;
	String headLabel;
	List<SyntaxNode> children = new ArrayList<>();
}
