package br.leg.senado.legimatica.googlenl.model;

import lombok.Data;

@Data
public class Text {
	private String content;
	private long beginOffset;
}