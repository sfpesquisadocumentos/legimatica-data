package br.leg.senado.legimatica.googlenl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PartOfSpeech {
	private String tag;
	private String aspect;
	@JsonProperty("case")
	private String textCase;
	private String form;
	private String gender;
	private String mood;
	private String number;
	private String person;
	private String proper;
	private String reciprocity;
	private String tense;
	private String voice;
	
}