package br.leg.senado.legimatica.googlenl.model;

import lombok.Data;

@Data
public class Sentence {
	private Text text;
}