package br.leg.senado.legimatica.googlenl.model;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class Entity {
	private String name;
	private String type;
	private double salience;
	private Map<String, String> metadata;
	private List<Mention> mentions;
}