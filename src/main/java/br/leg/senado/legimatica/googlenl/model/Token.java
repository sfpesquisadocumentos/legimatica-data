package br.leg.senado.legimatica.googlenl.model;

import lombok.Data;

@Data
public class Token {
	private Text text;
	private PartOfSpeech partOfSpeech;
	private DependencyEdge dependencyEdge;
	private String lemma;
}