package br.leg.senado.legimatica.googlenl.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Conll {
	
	private Conll() {
		// Utility class
	}
	
	/*
	 * Monta um CONLL-X para cada sentença reconhecida pelo parser do Google.
	 */
	public static List<String> toConllX(GoogleTextAnalysis textAnalysis) {
		
		List<String> conlls = new ArrayList<>();
		Iterator<Sentence> sentences = textAnalysis.getSentences().iterator();
		sentences.next(); // Pula a primeira sentença, só interessa a partir da segunda, se existir.
		Sentence nextSentence = null;
		long maxOffset = -1;
		
		StringBuilder conll = new StringBuilder();		
		List<Token> tokens = textAnalysis.getTokens();
		for (int i = 0; i < tokens.size(); i++) {
			Token token = tokens.get(i);
			
			if (token.getText().getBeginOffset() > maxOffset) {
				if (i > 0) {
					conlls.add(conll.toString());
					conll = new StringBuilder();
				}
				nextSentence = sentences.hasNext() ? sentences.next() : null;
				maxOffset = nextSentence == null ? Long.MAX_VALUE : nextSentence.getText().getBeginOffset() - 1;
			}
			
			conll.append(String.format("%s %s %s %s %s %s %s %s%n", 
					i+1,
					token.getText().getContent(),
					token.getLemma(),
					"_",
					token.getPartOfSpeech().getTag(),
					"_",
					token.getDependencyEdge().getHeadTokenIndex() == i ? "0" : (token.getDependencyEdge().getHeadTokenIndex() + 1),
					token.getDependencyEdge().getLabel().toLowerCase()
				));
		}
		conlls.add(conll.toString());
		return conlls;
	}	
}
