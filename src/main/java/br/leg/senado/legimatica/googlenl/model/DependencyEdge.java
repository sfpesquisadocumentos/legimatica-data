package br.leg.senado.legimatica.googlenl.model;

import lombok.Data;

@Data
public class DependencyEdge {
	private int headTokenIndex;
	private String label;
}