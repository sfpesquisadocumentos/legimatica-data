package br.leg.senado.legimatica.googlenl.model;

import lombok.Data;

@Data
public class Mention {
	private Text text;
	private String type;
}