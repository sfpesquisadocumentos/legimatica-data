package br.leg.senado.legimatica.googlenl.model;

import java.util.List;

import lombok.Data;

@Data
public class GoogleTextAnalysis {
	private List<Sentence> sentences;
	private List<Token> tokens;
	private List<Entity> entities;
	private String language;
}
