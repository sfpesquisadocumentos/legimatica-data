package br.leg.senado.legimatica;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.leg.senado.legimatica.corpus.CorpusBrowser;
import br.leg.senado.legimatica.sigen.SigenService;
import br.leg.senado.legimatica.sigen.model.Norma;
import br.leg.senado.legimatica.sigen.model.Termo;
import br.leg.senado.legimatica.sigen.parsers.ListaNormasCsvParser;
import lombok.extern.slf4j.Slf4j;

@Component
@RestController
@RequestMapping("/api/normas")
@Slf4j
public class NormasService {

	@Autowired
	ListaNormasCsvParser normasParser;

	@Autowired
	CorpusBrowser corpusBrowser;
	
	@Autowired
	SigenService sigenService;
	
	@Autowired
	TermosService termosService;
	
	private List<Norma> listaNormas;

	public List<Norma> getNormas() {
		if (this.listaNormas == null) {
			log.debug("Carregando lista de normas...");
			try(InputStream stream = getListaNormasCsvStream()) {
				this.listaNormas = normasParser.parse(stream);
				this.listaNormas.sort((n1, n2) -> n1.getDataAssinatura().compareTo(n2.getDataAssinatura()));
				log.debug(this.listaNormas.size() + " normas carregadas.");
			} catch (IOException e) {
				throw new NormasServiceException(e);
			}
		}
		return this.listaNormas;
	}

	protected InputStream getListaNormasCsvStream() {
		return Thread.currentThread().getContextClassLoader().getResourceAsStream("lista_leis.csv");
		//return new URL("http://pesquisa-textual.senado.gov.br/lista_leis.csv").openStream();
	}
	
	@GetMapping(path = "", produces = "application/json")
	public List<Norma> getNormas(
			@RequestParam(value="ano", required=false) Integer ano, 
			@RequestParam(value="limit", required=false) Integer limit) {
		if (ano == null && (limit == null || limit >= listaNormas.size())) {
			return this.getNormas();
		}
		return this.getNormas()
			.stream()
			.filter(n -> ano == null || 
						 ano == LocalDate.from(n.getDataAssinatura().toInstant().atZone(ZoneId.systemDefault())).getYear())
			.limit(limit == null ? listaNormas.size() : limit)
			.collect(Collectors.toList());
	}
	
	@GetMapping(path = "/nome/{nome}", produces = "application/json")
	public Norma getNormaPorNome(@PathVariable String nome) {
		if (nome == null || nome.isEmpty()) {
			throw new IllegalArgumentException("Nome precisa ser informado.");
		}
		return this.getNormas()
			.stream()
			.filter(n -> n.getNomeArquivo().startsWith(nome + "-"))
			.findFirst()
			.orElse(null);
	}
	
	public String getEmenta(
			@PathParam("tipoNorma") String tipoNorma,
			@PathParam("anoNorma") Integer anoNorma,
			@PathParam("numeroNorma") String numeroNorma) {
		return corpusBrowser.getTxtEmenta(anoNorma, tipoNorma, numeroNorma);
	}

	@SuppressWarnings("serial")
	static class NormasServiceException extends RuntimeException {
		NormasServiceException(Throwable t) {
			super(t.getMessage(), t);
		}
		NormasServiceException(String m) {
			super(m);
		}
	}

//	public Map<Termo, Set<String>> getTermosIndexadores(Long idNorma) {
//		Map<Termo, Set<String>> indexadores = new HashMap<>();
//		// Busca indexação do SIGEN
//		List<InstanciaRef> linksIndexacao = sigenService.getPropriedadeInstanciaRef(
//				idNorma, "relacaoDocumentoIndexacao"); 
//		if (linksIndexacao != null) {
//			// Para cada frase de Indexação
//			for (InstanciaRef link : linksIndexacao) {
//				List<InstanciaRef> linksFraseParticular = sigenService.getPropriedadeInstanciaRef(
//						Long.valueOf(link.getName()), "relacaoFraseIndexacaoParticular");
//				
//				// Obtém cada frase de indexação particular
//				for (InstanciaRef linkParticular : linksFraseParticular) {
//					merge(indexadores, encontraTermosIndexadores(linkParticular));
//				}
//			}
//		}		
//		return indexadores;
//	}
//
//	private Map<Termo, Set<String>> encontraTermosIndexadores(InstanciaRef instanciaRef) {
//		Map<Termo, Set<String>> termos = new HashMap<>();
//		
//		Termo termo = termosService.localizaTermoPorId(Long.valueOf(instanciaRef.getName()));
//		if (termo == null) {
//			List<InstanciaRef> termosPreferenciais = sigenService.getPropriedadeInstanciaRef(
//					Long.valueOf(instanciaRef.getName()), "denotadoPreferencialmentePor");
//			for (InstanciaRef ref : termosPreferenciais) {
//				// TODO Incluir controle para não ter loop infinito
//				merge(termos, encontraTermosIndexadores(ref));
//			}
//			List<InstanciaRef> termosAlternativos = sigenService.getPropriedadeInstanciaRef(
//					Long.valueOf(instanciaRef.getName()), "denotadoAlternativamentePor"); 
//			if (termosAlternativos != null) {
//				for (InstanciaRef refAlt : termosAlternativos) {
//					// TODO Incluir controle para não ter loop infinito
//					merge(termos, encontraTermosIndexadores(refAlt));
//				}
//			}
//		} else {
//			log.debug("Termo {}, id {}, tipo {}", termo.getTexto(), termo.getId(), termo.getP());
//			merge(termos, encontraNomeESiglaTermos(termo, instanciaRef));
//		}
//		return termos;
//	}
//
//	private Map<Termo, Set<String>> encontraNomeESiglaTermos(Termo termo, InstanciaRef instanciaRef) {
//		Map<Termo, Set<String>> termos = new HashMap<>();
//		String nomeTermo = sigenService.getPropriedade(Long.valueOf(instanciaRef.getName()), "nomeDeTermo", String.class);
//		if (nomeTermo != null) {
//			append(termos, termo, nomeTermo.trim());
//			String siglaTermo = sigenService.getPropriedade(Long.valueOf(instanciaRef.getName()), "siglaDeTermo", String.class);
//			if (siglaTermo != null) {
//				append(termos, termo, siglaTermo.trim().replaceAll("[\\(\\)]", "")); // Remove os parênteses da sigla 
//			}
//			log.debug("  Expressão {}, sigla {}, id {}, tipo {}", nomeTermo, siglaTermo, instanciaRef.getName(), termo.getP());
//		} else {
//			List<InstanciaRef> termosPreferenciais =
//					sigenService.getPropriedadeInstanciaRef(Long.valueOf(instanciaRef.getName()), "denotadoPreferencialmentePor");
//			if (termosPreferenciais != null) {
//				for (InstanciaRef ref : termosPreferenciais) {
//					// TODO Incluir controle para não ter loop infinito
//					merge(termos, encontraNomeESiglaTermos(termo, ref));
//				}
//			}
//			List<InstanciaRef> termosAlternativos =
//					sigenService.getPropriedadeInstanciaRef(Long.valueOf(instanciaRef.getName()), "denotadoAlternativamentePor");
//			if (termosAlternativos != null) {
//				for (InstanciaRef refAlt : termosAlternativos) {
//					// TODO Incluir controle para não ter loop infinito
//					merge(termos, encontraNomeESiglaTermos(termo, refAlt));
//				}
//			}
//		}
//		return termos;
//	}

	private void merge(Map<Termo, Set<String>> map, Map<Termo, Set<String>> newValues) {
		for (Entry<Termo, Set<String>> entry : newValues.entrySet()) {
			append(map, entry.getKey(), entry.getValue());
		}
	}
	
	private void append(Map<Termo, Set<String>> map, Termo termo, Set<String> expressoes) {
		if (map.containsKey(termo)) {
			map.get(termo).addAll(expressoes);
		} else {
			map.put(termo, new HashSet<>(expressoes));
		}		
	}

	private void append(Map<Termo, Set<String>> map, Termo termo, String expressao) {
		append(map, termo, Collections.singleton(expressao));
	}
/*	
	public Map<String, Termo> getIndexadores(Long idNorma) {
		
		Map<String, Termo> indexadores = new HashMap<>();
		
		// Busca indexação do SIGEN
		List<InstanciaRef> linksIndexacao = sigenService.getPropriedadeInstanciaRef(
				idNorma, "relacaoDocumentoIndexacao"); 
		if (linksIndexacao != null) {
			// Para cada frase de Indexação
			for (InstanciaRef link : linksIndexacao) {
				List<InstanciaRef> linksFraseParticular = sigenService.getPropriedadeInstanciaRef(
						Long.valueOf(link.getName()), "relacaoFraseIndexacaoParticular");
				
				// Obtém cada frase de indexação particular
				for (InstanciaRef linkParticular : linksFraseParticular) {
					indexadores.putAll(encontraTermos(linkParticular));
				}
			}
		}
		return indexadores;
	}

	private Map<String, Termo> encontraTermos(InstanciaRef instanciaRef) {
		Map<String, Termo> termos = new HashMap<>();
		
		Termo termo = termosService.localizaTermoPorId(Long.valueOf(instanciaRef.getName()));
		if (termo == null) {
			List<InstanciaRef> termosPreferenciais = sigenService.getPropriedadeInstanciaRef(
					Long.valueOf(instanciaRef.getName()), "denotadoPreferencialmentePor");
			for (InstanciaRef ref : termosPreferenciais) {
				// TODO Incluir controle para não ter loop infinito
				termos.putAll(encontraTermos(ref));
			}
			List<InstanciaRef> termosAlternativos = sigenService.getPropriedadeInstanciaRef(
					Long.valueOf(instanciaRef.getName()), "denotadoAlternativamentePor"); 
			if (termosAlternativos != null) {
				for (InstanciaRef refAlt : termosAlternativos) {
					// TODO Incluir controle para não ter loop infinito
					termos.putAll(encontraTermos(refAlt));
				}
			}
		} else {
			log.debug("Termo {}, id {}, tipo {}", termo.getTexto(), termo.getId(), termo.getP());
			termos.putAll(encontraNomeESigla(termo, instanciaRef));
		}
		return termos;
	}

	private Map<String, Termo> encontraNomeESigla(Termo termo, InstanciaRef instanciaRef) {
		Map<String, Termo> termos = new HashMap<>();
		String nomeTermo = sigenService.getPropriedade(Long.valueOf(instanciaRef.getName()), "nomeDeTermo", String.class);
		if (nomeTermo != null) {
			termos.put(nomeTermo.trim(), termo);
			String siglaTermo = sigenService.getPropriedade(Long.valueOf(instanciaRef.getName()), "siglaDeTermo", String.class);
			if (siglaTermo != null) {
				termos.put(siglaTermo.trim().replaceAll("[\\(\\)]", ""), termo); // Remove os parênteses da sigla 
			}
			log.debug("  Expressão {}, sigla {}, id {}, tipo {}", nomeTermo, siglaTermo, instanciaRef.getName(), termo.getP());
		} else {
			List<InstanciaRef> termosPreferenciais =
					sigenService.getPropriedadeInstanciaRef(Long.valueOf(instanciaRef.getName()), "denotadoPreferencialmentePor");
			if (termosPreferenciais != null) {
				for (InstanciaRef ref : termosPreferenciais) {
					// TODO Incluir controle para não ter loop infinito
					termos.putAll(encontraNomeESigla(termo, ref));
				}
			}
			List<InstanciaRef> termosAlternativos =
					sigenService.getPropriedadeInstanciaRef(Long.valueOf(instanciaRef.getName()), "denotadoAlternativamentePor");
			if (termosAlternativos != null) {
				for (InstanciaRef refAlt : termosAlternativos) {
					// TODO Incluir controle para não ter loop infinito
					termos.putAll(encontraNomeESigla(termo, refAlt));
				}
			}
		}
		return termos;
	}
*/
}
