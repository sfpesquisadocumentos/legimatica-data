package br.leg.senado.legimatica.util;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.StdTypeResolverBuilder;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Component
public class GenericJsonProcessor {
	
	private ObjectMapper objectMapper;

	@SuppressWarnings("serial")
	protected static class JsonProcessingException extends RuntimeException {
		public JsonProcessingException(Throwable cause) {
			super(cause.getMessage(), cause);
		}
	}

	public <T> T parse(InputStream jsonStream, Class<T> objectClass) {
		T result = null;
		try {
			result = getObjectMapper().readValue(jsonStream, objectClass);
		} catch (IOException e) {
			throw new JsonProcessingException(e);
		}
		return result;
	}

	public <T> T parseProperty(InputStream jsonStream, String propertyName, Class<T> objectClass) {
		T result = null;
		try {
			JsonNode jsonTree = getObjectMapper().readTree(jsonStream);
			JsonNode jsonProperty = jsonTree.get(propertyName);
			if (jsonProperty == null || jsonProperty.isNull()) {
				return null;
			}
			result = getObjectMapper().treeToValue(jsonProperty, objectClass);
		} catch (IOException e) {
			throw new JsonProcessingException(e);
		}
		return result;
	}
	
	public ObjectMapper getObjectMapper() {
		if (objectMapper == null) {
			Jackson2ObjectMapperBuilder mapperBuilder = new Jackson2ObjectMapperBuilder();
	
			// Configurando o default Typing para property @class
			StdTypeResolverBuilder typeResolverBuilder = new ObjectMapper.DefaultTypeResolverBuilder(
					ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT);
			typeResolverBuilder = typeResolverBuilder.inclusion(JsonTypeInfo.As.PROPERTY);
			typeResolverBuilder.init(JsonTypeInfo.Id.CLASS, null);
			mapperBuilder.defaultTyping(typeResolverBuilder);
	
			//b.indentOutput(true);
	
			// Datas no format ISO 8601
			mapperBuilder.dateFormat(new ISO8601DateFormat());
	
			// Não falha ao deserializar um objeto que contenha propriedades  desconhecidas
			mapperBuilder.featuresToDisable(
					DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
					DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES);
	
			mapperBuilder.featuresToEnable(
					DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
	
			mapperBuilder.modulesToInstall(new JavaTimeModule());
			objectMapper = mapperBuilder.build();
		}
		return objectMapper;
	}
}

