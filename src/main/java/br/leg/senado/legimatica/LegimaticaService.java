package br.leg.senado.legimatica;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.leg.senado.legimatica.googlenl.model.GoogleTextAnalysis;
import br.leg.senado.legimatica.googlenl.model.Mention;
import br.leg.senado.legimatica.sigen.SigenService;
import br.leg.senado.legimatica.sigen.model.Norma;

@Component
@RestController
@RequestMapping("/api/analise")
public class LegimaticaService {
	
	@Autowired
	SigenService sigenService;
	
	@Autowired
	TermosService termosService;
	
	@Autowired
	NormasService normasService;

	@Autowired
	AnaliseTextualService analiseTextualService;
	
	@GetMapping(path = "/ano/{ano:[\\d]+}", produces = "application/json")
	public void analisaNormas(@PathVariable Integer ano, @RequestParam(name="limit", required=false) Integer limit) {
		List<Norma> normas = normasService.getNormas(ano, limit);
		for (Norma norma : normas) {
			analisaNorma(norma);
		}
	}

	@GetMapping(path = "/norma/nome/{nome}", produces = "application/json")
	public void analisaNorma(@PathVariable String nome) {
		analisaNorma(normasService.getNormaPorNome(nome));
	}
	
	public void analisaNorma(Norma norma) {
				
		try {
			Map<String, GoogleTextAnalysis> analise = analiseTextualService.getAnaliseNorma(norma);
			for (String key : analise.keySet()) {
				GoogleTextAnalysis analiseSentenca = analise.get(key);
				List<String> entidades = analiseSentenca.getEntities().stream()
//						.filter(e -> "PROPER".equals(e.getType()))
						.map(/*Entity::getName*/ e -> e.getName() + "/" + e.getMentions().stream().map(Mention::getType).collect(Collectors.toList()))
						.collect(Collectors.toList());
				System.out.println("> " + entidades);
			}
		} catch (Exception e) {
			System.out.println("> Não foi possível obter a análise textual: " + e.getMessage());
		}

//		InstanciaCompacta instanciaNorma = sigenService.getDetalheInstancia(norma.getCodDocumento().toString());
//		System.out.println(instanciaNorma);

	}
	
}
