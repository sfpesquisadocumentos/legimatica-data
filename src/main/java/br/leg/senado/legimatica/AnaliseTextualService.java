package br.leg.senado.legimatica;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.leg.senado.legimatica.corpus.CorpusBrowser;
import br.leg.senado.legimatica.googlenl.model.GoogleTextAnalysis;
import br.leg.senado.legimatica.googlenl.parsers.GoogleCloudResultJsonParser;
import br.leg.senado.legimatica.sigen.model.Norma;

@Component
@RestController
@RequestMapping("/api/analise-textual")
public class AnaliseTextualService {

	@Autowired
	GoogleCloudResultJsonParser parser;
	
	@Autowired
	CorpusBrowser browser;	

	public Map<String, GoogleTextAnalysis> getAnaliseNorma(Norma norma) {
		return getAnaliseNorma(norma.getTipo(), norma.getAno(), norma.getNumero());
	}
	
	@GetMapping(path = "/{tipoNorma}-{anoNorma}-{numeroNorma}", produces = "application/json")
	public Map<String, GoogleTextAnalysis> getAnaliseNorma(
			@PathParam("tipoNorma") String tipoNorma,
			@PathParam("anoNorma") Integer anoNorma,
			@PathParam("numeroNorma") String numeroNorma) {

		Map<String, GoogleTextAnalysis> analises = new LinkedHashMap<>();
//		List<String> dispositivos = browser.listaDispositivos(anoNorma, tipoNorma, numeroNorma);
//		for (String dispositivo : dispositivos) {
//			GoogleTextAnalysis analise = getAnaliseDispositivo(tipoNorma, anoNorma, numeroNorma, dispositivo);
//			analises.put(dispositivo, analise);
//		}
		
		analises.put("Ementa", getAnaliseEmenta(tipoNorma, anoNorma, numeroNorma));
		analises.put("Corpo", getAnaliseCorpoNorma(tipoNorma, anoNorma, numeroNorma));
		
		return analises;
	}
	
	@GetMapping(path = "/{tipoNorma}-{anoNorma}-{numeroNorma}/{idDispositivo}", produces = "application/json")
	public GoogleTextAnalysis getAnaliseDispositivo(
			@PathParam("tipoNorma") String tipoNorma,
			@PathParam("anoNorma") Integer anoNorma,
			@PathParam("numeroNorma") String numeroNorma,
			@PathParam("idDispositivo") String idDispositivo) {

		InputStream jsonStream = browser.getJsonDispositivo(anoNorma, tipoNorma, numeroNorma, idDispositivo);
		return parser.parse(jsonStream);
	}

	public GoogleTextAnalysis getAnaliseEmenta(
			@PathParam("tipoNorma") String tipoNorma,
			@PathParam("anoNorma") Integer anoNorma,
			@PathParam("numeroNorma") String numeroNorma) {

		InputStream jsonStream = browser.getJsonEmenta(anoNorma, tipoNorma, numeroNorma);
		return parser.parse(jsonStream);
	}

	public GoogleTextAnalysis getAnaliseCorpoNorma(
			@PathParam("tipoNorma") String tipoNorma,
			@PathParam("anoNorma") Integer anoNorma,
			@PathParam("numeroNorma") String numeroNorma) {

		InputStream jsonStream = browser.getJsonNorma(anoNorma, tipoNorma, numeroNorma);
		return parser.parse(jsonStream);
	}
	
}
